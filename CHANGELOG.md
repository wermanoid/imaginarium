# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.1.0](http://bitbucket.org/wermanoid/imaginarium/compare/v2.0.0...v2.1.0) (2019-12-08)


### Features

* **about:** add page with tech stack description ([7aa01d3](http://bitbucket.org/wermanoid/imaginarium/commit/7aa01d3f4ea1e4a3aa2529fb8692ec32ce87a484)), closes [#IMG-9](https://this.is.my/custom/tracker/issue/IMG-9)

## 2.0.0 (2019-12-08)


### ⚠ BREAKING CHANGES

* **application:** Aplication work. Time to release

### Features

* **application:** different updates ([d6c7504](http://bitbucket.org/wermanoid/imaginarium/commit/d6c7504b0c4a9728873a28a9085c4c8cd60760ce)), closes [#IMG-3](https://this.is.my/custom/tracker/issue/IMG-3) [#IMG-4](https://this.is.my/custom/tracker/issue/IMG-4)
* **application:** running version added ([3809156](http://bitbucket.org/wermanoid/imaginarium/commit/380915692a084b03c5c55c0df16a45e3cddd769b)), closes [#IMG-02](https://this.is.my/custom/tracker/issue/IMG-02)
* **edit:** add imageDetails and update graphql ([93a07c5](http://bitbucket.org/wermanoid/imaginarium/commit/93a07c50587fa67151ce4f4fa8f1928f3ccbb879))
* **images:** add store and related infra ([c6854c3](http://bitbucket.org/wermanoid/imaginarium/commit/c6854c3a4c9629e485ec00633a710ec3e6a77105)), closes [#IMG-01](https://this.is.my/custom/tracker/issue/IMG-01)


### Bug Fixes

* **name:** change project name ([786c71e](http://bitbucket.org/wermanoid/imaginarium/commit/786c71ee548cca269b39d7688701d0de792a4ee1)), closes [#IMG-5](https://this.is.my/custom/tracker/issue/IMG-5)


### Documents

* **readme:** update readme docs ([d68fb3a](http://bitbucket.org/wermanoid/imaginarium/commit/d68fb3a8b2369a725c1274221fc20f9122e5e819)), closes [#IMG-6](https://this.is.my/custom/tracker/issue/IMG-6)
