# Imaginarium exercise

## Pre-requirements

Server is written using TypeScript + Express + Graphql + Apollo

Client-side technology stack is TypeScript + React + MobX + Apollo

Application itself should be started by NodeJS and Yarn:

| App/Lib    | Version     |
| ---------- | ----------- |
| NodeJs     | LTS (12.13) |
| Typescript | >= 3.7.2    |
| Yarn       | >= 1.12.0   |

## How to start

Simply clone this repository with `git clone` or other preferable way and follow next steps:

- install dependencies

```bash
yarn
```

- Better to execure first run with DB initialization, so application could bootstrap images store and database:

```bash
yarn start:initial

#or

yarn generate-db && yarn start
```

- start application in development mode

```bash
yarn start
```

- build production ready version

```bash
yarn build
```

- start application in production mode

```bash
yarn start:prod
```

Wait until application starts (usually it takes around 5-10 seconds on first start) and it will be available on `http://localhost:9000`.

## Release

Application use [standard-version](https://github.com/conventional-changelog/standard-version) to handle automated release process.

Add new conventional-commit compatible commit. Process for conventional commits is also automated:

```bash
yarn commit
# issue format for last question is like:  Closes #IMG-7
```

and later:

```bash
#for test check
yarn standard-version --dry-run

#to mark actual release
yarn standard-version
```

## Storybook

This repository contains `storybook` for developing/presenting components. To run it just hit

```bash
yarn storybook
```

Wait for it and it will be opened automatically in the new tab (in case of need it's available on `http://localhost:6006`)

## Testing

To test whole application just run

```bash
yarn test
```

It will run `jest`, `tslint` and `tsc` and print the results to the standard output.
