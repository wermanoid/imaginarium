import { createGzip } from 'zlib';
import request from 'request';
import fs from 'fs';
import path from 'path';

const imgList = [
  {
    src: 'https://cdn.pixabay.com/photo/2017/02/20/18/03/cat-2083492__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2017/02/20/18/03/cat-2083492__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2019/11/08/11/56/cat-4611189__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2019/11/08/11/56/cat-4611189__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634__340.png',
    srcset:
      'https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634__480.png 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2015/03/27/13/16/cat-694730__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2015/03/27/13/16/cat-694730__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2015/11/16/14/43/cat-1045782__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2015/11/16/14/43/cat-1045782__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2015/04/23/21/59/tree-736877__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2015/04/23/21/59/tree-736877__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2014/11/30/14/11/kitty-551554__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2014/11/30/14/11/kitty-551554__480.jpg 2x',
  },
  {
    src: 'https://cdn.pixabay.com/photo/2014/04/13/20/49/cat-323262__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2014/04/13/20/49/cat-323262__480.jpg 2x',
  },
  {
    src:
      'https://cdn.pixabay.com/photo/2017/07/24/19/57/tiger-2535888__340.jpg',
    srcset:
      'https://cdn.pixabay.com/photo/2017/07/24/19/57/tiger-2535888__480.jpg 2x',
  },
];

const dbJS: any = {
  images: [],
};

imgList.map((item, id) => {
  const name = `cat-${id + 1}`;
  const ext1 = item.src.split('.');
  const ext2 = item.srcset.split('.');

  const x1Name = `${name}-1x.${ext1[ext1.length - 1]}`;
  const x2Name = `${name}-2x.${ext2[ext2.length - 1].split(' ')[0]}`;
  dbJS.images.push({
    src: `/image/${x1Name}`,
    srcset: `/image/${x2Name} 2x`,
    files: [`db/${x1Name}.gz`, `db/${x2Name}.gz`],
  });

  request(item.src)
    .pipe(createGzip())
    .pipe(
      fs.createWriteStream(path.resolve(process.cwd(), `./db/${x1Name}.gz`), {
        flags: 'w',
      })
    );

  request(item.src)
    .pipe(createGzip())
    .pipe(fs.createWriteStream(path.resolve(process.cwd(), `db/${x2Name}.gz`)));
});

const dbInfo = path.resolve(process.cwd(), './db/data.json');
fs.writeFile(dbInfo, JSON.stringify(dbJS, null, 2), { flag: 'w' }, () => {});
