import { action, observable } from 'mobx';
import { List } from 'purify-ts/List';
import { EitherAsync } from 'purify-ts/EitherAsync';
import { Right, Left } from 'purify-ts/Either';

import { ImageData } from '#shared/types';

import { ApolloSource } from '../data-source/apollo-source';

const findById = <T extends { id: string }>(id: string, items: T[]) =>
  List.find((item: T) => item.id === id, items).toEither(null);

export class ImagesStore {
  @observable public data: ImageData[];
  private source: ApolloSource;

  constructor(source: ApolloSource, initial?: ImageData[]) {
    this.data = initial || [];
    this.source = source;
  }

  public async init() {
    return this.source.getImages().then(({ data }) => {
      this.data = data.images;
    });
  }

  private loadImageFromSource = async (id: string) => {
    const result = await this.source.getImageById(id);
    if (result.errors) return Left(result.errors);
    return Right(result.data.image);
  };

  public getOneById = async (id: string): Promise<ImageData> =>
    ((
      await EitherAsync(async ({ fromPromise }) =>
        findById(id, this.data)
          .chain(item => (item.created ? Right(item) : Left(null)))
          .mapLeft(async () => await fromPromise(this.loadImageFromSource(id)))
          .extract()
      ).run()
    ).extract() as unknown) as Promise<ImageData>;

  @action public updateImageTitle = (image: ImageData) => {
    const imageIndex = this.data.findIndex(item => item.id === image.id);
    if (imageIndex > -1) this.data[imageIndex].title = image.title;
    return this.source.updateImageTitle(image);
  };

  @action public removeImage = (image: ImageData) => {
    this.data = this.data.filter(item => item.id !== image.id);
    return this.source.removeImage(image);
  };

  public toObject = () => ({ images: this.data });
}
