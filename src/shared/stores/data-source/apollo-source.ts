import { ImageData } from '#shared/types';
import ApolloClient, { gql } from 'apollo-boost';

export class ApolloSource {
  private client: ApolloClient<{}>;

  constructor(client: ApolloClient<{}>) {
    this.client = client;
  }

  public updateImageTitle = ({ id, title }: { title: string; id: string }) => {
    return this.client.mutate<boolean>({
      mutation: gql`
        mutation Update($upd: UpdateTitleArgs!) {
          updaetTitle(upd: $upd)
        }
      `,
      variables: {
        upd: { id, title },
      },
    });
  };

  public removeImage = ({ id }: { id: string }) => {
    return this.client.mutate<boolean>({
      mutation: gql`
        mutation Update($id: String!) {
          removeImage(id: $id)
        }
      `,
      variables: {
        id,
      },
    });
  };

  public getImages = async () =>
    this.client.query<{ images: ImageData[] }>({
      query: gql`
        query Main {
          images {
            id
            title
            src
            srcset
            author
          }
        }
      `,
    });

  public getImageById = async (id: string) =>
    this.client.query<{ image: ImageData }>({
      query: gql`
        query Main($id: String!) {
          image(id: $id) {
            id
            title
            author
            src
            srcset
            created
          }
        }
      `,
      variables: { id },
    });
}
