import { ApolloClient } from 'apollo-boost';
import { RouterStore } from 'mobx-react-router';

import { ApolloSource } from './data-source';
import { ImagesStore } from './images';

export interface Stores {
  routing: RouterStore;
  images: ImagesStore;
}

export interface ServerSideStores extends Stores {
  toObject(): { [k in keyof Stores]: object };
}

export const createStores = async (
  client: ApolloClient<{}>,
  initial?: { [k in keyof Stores]: any } // eslint-disable-line @typescript-eslint/no-explicit-any
): Promise<ServerSideStores> => {
  const source = new ApolloSource(client);
  const routingStore = new RouterStore();
  const imagesStore = new ImagesStore(source, initial?.images);

  await imagesStore.init();

  return {
    routing: routingStore,
    images: imagesStore,
    toObject() {
      return {
        routing: routingStore,
        images: imagesStore.toObject(),
      };
    },
  };
};
