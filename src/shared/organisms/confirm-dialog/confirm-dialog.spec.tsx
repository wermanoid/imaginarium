import React from 'react';
import { mount } from 'enzyme';

import { ConfirmDialog } from './confirm-dialog';

const filterByMessage = (message: string) => n => n.text() === message;

describe('confirm-dialog', () => {
  it('shoud should render dialog when opened', () => {
    const testMessage = 'This is test message';
    const wrapper = mount(
      <ConfirmDialog
        open
        message={testMessage}
        onConfirm={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    const result = wrapper.find('p').filterWhere(filterByMessage(testMessage));
    expect(result).toHaveLength(1);
  });

  it('shoud should not render dialog when opened', () => {
    const testMessage = 'This is test message';
    const wrapper = mount(
      <ConfirmDialog
        open={false}
        message={testMessage}
        onConfirm={jest.fn()}
        onCancel={jest.fn()}
      />
    );

    const result = wrapper.find('p').filterWhere(filterByMessage(testMessage));
    expect(result).toHaveLength(0);
  });

  it('shoud should submit dialog', () => {
    const testMessage = 'This is test message';
    const sumbmitSpy = jest.fn();
    const wrapper = mount(
      <ConfirmDialog
        open
        message={testMessage}
        onConfirm={sumbmitSpy}
        onCancel={jest.fn()}
      />
    );

    const confirm = wrapper
      .find('button')
      .filterWhere(filterByMessage('Confirm'));
    confirm.simulate('click');

    expect(sumbmitSpy).toHaveBeenCalled();
  });

  it('shoud should submit cancel', () => {
    const testMessage = 'This is test message';
    const cancelSpy = jest.fn();
    const wrapper = mount(
      <ConfirmDialog
        open
        message={testMessage}
        onConfirm={jest.fn()}
        onCancel={cancelSpy}
      />
    );

    const cancel = wrapper
      .find('button')
      .filterWhere(filterByMessage('Cancel'));
    cancel.simulate('click');

    expect(cancelSpy).toHaveBeenCalled();
  });
});
