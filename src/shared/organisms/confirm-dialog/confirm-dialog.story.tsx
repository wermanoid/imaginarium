import React from 'react';
import { text, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';

import { ConfirmDialog } from './confirm-dialog';

storiesOf('[Organisms]|ConfirmDialog', module).add('default', () => (
  <ConfirmDialog
    message={text('Message', 'Confirmation question message')}
    open={boolean('Open?', true)}
    onCancel={action('Cancelled')}
    onConfirm={action('Confirmed')}
  />
));
