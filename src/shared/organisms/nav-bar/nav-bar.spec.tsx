import React from 'react';
import { mount, ReactWrapper } from 'enzyme';

import { NavBarComponent } from './nav-bar';

// declare global {
//   namespace NodeJS {
interface Global extends NodeJS.Global {
  mockResponsiveResult: boolean;
}
//   }
// }

jest.mock('../../utils', () => ({
  useResponsive: jest
    .fn()
    .mockImplementation(() => (global as Global).mockResponsiveResult),
  withInject: () => jest.fn(),
}));

const byTestId = (testId: string, wrapper: ReactWrapper) =>
  wrapper.find(`[data-test-id="${testId}"]`);

const setDeviceMod = (isMobile: boolean) => {
  (global as Global).mockResponsiveResult = isMobile;
};

describe('nav-bar', () => {
  beforeEach(() => {
    setDeviceMod(false);
  });

  afterAll(() => {
    delete (global as Global).mockResponsiveResult;
  });

  it('should render in desktop mode', () => {
    const wrapper = mount(
      <NavBarComponent title="test-app" goTo={() => jest.fn()} actualPath="/" />
    );

    const ImageIcon = byTestId('header-icon-image', wrapper).find('svg');
    const MenuIcon = byTestId('header-icon-menu', wrapper).find('svg');

    expect(ImageIcon).toHaveLength(1);
    expect(MenuIcon).toHaveLength(0);
  });

  it('should render in mobile mode', () => {
    setDeviceMod(true);
    const wrapper = mount(
      <NavBarComponent title="test-app" goTo={() => jest.fn()} actualPath="/" />
    );

    const ImageIcon = byTestId('header-icon-image', wrapper).find('svg');
    const MenuIcon = byTestId('header-icon-menu', wrapper).find('svg');

    expect(ImageIcon).toHaveLength(0);
    expect(MenuIcon).toHaveLength(1);
  });

  it('should have permanent navigation in desktop mode', () => {
    const wrapper = mount(
      <NavBarComponent title="test-app" goTo={() => jest.fn()} actualPath="/" />
    );

    const drawer = byTestId('header-drawer', wrapper).first();

    const resultProps = drawer.props();
    expect(resultProps).toHaveProperty('open', true);
    expect(resultProps).toHaveProperty('variant', 'permanent');
  });

  it('should open navigation in mobile mode', () => {
    setDeviceMod(true);
    const wrapper = mount(
      <NavBarComponent title="test-app" goTo={() => jest.fn()} actualPath="/" />
    );

    const openButton = byTestId('header-icon-button', wrapper).find('button');
    openButton.simulate('click');

    const drawer = byTestId('header-drawer', wrapper).first();

    const resultProps = drawer.props();
    expect(resultProps).toHaveProperty('open', true);
    expect(resultProps).toHaveProperty('variant', 'persistent');
  });
});
