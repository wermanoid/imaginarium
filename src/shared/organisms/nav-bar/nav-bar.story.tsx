import React from 'react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';
import orange from '@material-ui/core/colors/orange';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

import { NavBarComponent } from './nav-bar';

storiesOf('[Organisms]|NavBar', module)
  .addParameters({ viewport: { viewports: INITIAL_VIEWPORTS } })
  .addDecorator(story => <ThemeProvider theme={theme}>{story()}</ThemeProvider>)
  .add('default', () => {
    const props = {
      title: text('Title', 'Navigation Layot'),
      goTo: action('Navigation event') as (
        url: string,
        cb?: Function
      ) => (e: React.SyntheticEvent) => void,
      actualPath: '/images',
    };

    return <NavBarComponent {...props} />;
  });
