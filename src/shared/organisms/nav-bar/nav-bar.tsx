import React, { useState } from 'react';
import {
  List,
  AppBar,
  Drawer,
  Divider,
  ListItem,
  IconButton,
  Typography,
} from '@material-ui/core';
import { Image, Menu, Close } from '@material-ui/icons';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import { makeStyles } from '@material-ui/core/styles';

import {
  RoutingMap,
  useResponsive,
  withInject,
  mapRoutingStore,
} from '#shared/utils';
import { NavigationBar } from '#shared/atoms/navigation-bar';
import { NavigationBarHeader } from '#shared/molecules/navigation-bar-header';
import { observer } from 'mobx-react';

const drawerWidth = '250px';

const useStyles = makeStyles(theme => ({
  selectedNav: {
    position: 'relative',
    '&::before': {
      content: '""',
      width: '2px',
      background: 'red',
      position: 'absolute',
      left: 0,
      top: 0,
      bottom: 0,
    },
  },
  [theme.breakpoints.up('sm')]: {
    appBar: {
      width: `calc(100% - ${drawerWidth})`,
      marginLeft: drawerWidth,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
}));

export const NavBarComponent: React.FC<{
  title: string;
  goTo: (url: string, cb?: Function) => (e: React.SyntheticEvent) => void;
  actualPath: string;
}> = ({ goTo, actualPath, title }) => {
  const isMobile = useResponsive(
    [
      {
        query: (theme: Theme) => theme.breakpoints.down('xs'),
        value: true,
      },
    ],
    false
  );

  const [navOpen, setNavOpen] = useState(false);
  const closeDrawer = () => setNavOpen(false);
  const { selectedNav, appBar, drawer } = useStyles();
  const getActiveItemClass = (active: boolean) => {
    if (active) return selectedNav;
    return;
  };

  return (
    <>
      <AppBar position="fixed" className={appBar}>
        <NavigationBar>
          <NavigationBarHeader
            title={title}
            icon={
              <IconButton
                onClick={() => isMobile && setNavOpen(true)}
                color="inherit"
                data-test-id="header-icon-button"
              >
                {isMobile ? (
                  <Menu data-test-id="header-icon-menu" />
                ) : (
                  <Image data-test-id="header-icon-image" />
                )}
              </IconButton>
            }
          />
        </NavigationBar>
      </AppBar>
      <Drawer
        open={isMobile ? navOpen : true}
        variant={isMobile ? 'persistent' : 'permanent'}
        anchor="left"
        className={drawer}
        data-test-id="header-drawer"
      >
        <div css={{ width: '250px', display: 'flex', paddingLeft: '1rem' }}>
          <Typography variant="h4" css={{ flex: 1 }} data-test-id="nav-title">
            Navigation
          </Typography>
          {isMobile && (
            <IconButton onClick={closeDrawer} data-test-id="nav-close">
              <Close />
            </IconButton>
          )}
        </div>
        <Divider />
        <List>
          <ListItem
            button
            onClick={goTo('/', closeDrawer)}
            className={getActiveItemClass(actualPath === '/')}
            data-test-id="go-home"
          >
            Home
          </ListItem>
          <ListItem
            button
            onClick={goTo('/images', closeDrawer)}
            className={getActiveItemClass(actualPath === '/images')}
            data-test-id="go-images"
          >
            Images
          </ListItem>
          <ListItem
            button
            onClick={goTo('/about', closeDrawer)}
            className={getActiveItemClass(actualPath === '/about')}
            data-test-id="go-about"
          >
            About
          </ListItem>
        </List>
      </Drawer>
    </>
  );
};

export default withInject<RoutingMap>([mapRoutingStore])(
  observer(NavBarComponent)
);
