import React from 'react';
import { Route, Switch } from 'react-router';

import { Images, NotFound, ImageDetails, About } from '#shared/pages';
import { RoutesEnum } from './constants';

const Router: React.FC = () => (
  <>
    <Switch>
      <Route exact path={RoutesEnum.Home} component={Images} />
      <Route path={RoutesEnum.ImageDetails} component={ImageDetails} />
      <Route path={RoutesEnum.Images} component={Images} />
      <Route path={RoutesEnum.About} component={About} />
      <Route path="*" component={NotFound} />
    </Switch>
  </>
);

export default Router;
