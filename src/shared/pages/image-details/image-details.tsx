import React, { useState, useEffect } from 'react';
import { observer, useLocalStore } from 'mobx-react';
import {
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Typography,
  CardMedia,
  CardActions,
  TextField,
} from '@material-ui/core';
import { Edit, Delete, Save, CancelOutlined } from '@material-ui/icons';

import {
  withInject,
  InjectionMapper,
  mapRoutingStore,
  RoutingMap,
} from '#shared/utils';
import { ImageData } from '#shared/types';
import { ConfirmDialog } from '#shared/organisms/confirm-dialog';

interface DetailsStore {
  image: ImageData | null;
  loading: boolean;
  titleUpdate: string;
  setImage: (result: ImageData) => void;
}

interface ImageDetailsMobxContainer extends RoutingMap {
  getOne: (id: string) => Promise<ImageData>;
  updateTitle: (image: ImageData) => void;
  removeImage: (image: ImageData) => Promise<void>;
}

const compose = <T1, T2, R>(f: (x: T2) => R, g: (x?: T1) => T2) => (x?: T1) =>
  f(g(x));
const toggleCall = (f: (x: boolean) => void, arg: boolean) => () => f(arg);

export const ImageDetailsComponent: React.FC<ImageDetailsMobxContainer & {
  match: { params: { id: string } };
}> = ({ getOne, updateTitle, match, goTo, removeImage }) => {
  const [isEdit, setEdit] = useState(false);
  const [toRemove, setToRemove] = useState(false);
  const store = useLocalStore<DetailsStore>(() => ({
    image: null,
    titleUpdate: '',
    loading: true,
    setImage(value) {
      this.image = value;
      this.loading = false;
    },
  }));

  useEffect(() => {
    getOne(match.params.id)
      .then(result => store.setImage(result))
      .catch(() => {
        store.loading = false;
      });
  }, []);

  if (store.loading) {
    return <Typography>Loading info...</Typography>;
  }

  const { image } = store;
  if (image === null) {
    return <div>Image not Found</div>;
  }

  const onSave = compose(updateTitle, () => {
    store.image = { ...image, title: store.titleUpdate };
    setEdit(false);
    return store.image;
  });

  const onRemove: () => void = compose(() => {
    store.image && removeImage(store.image).then(() => goTo('/images')());
  }, toggleCall(setToRemove, false));

  return (
    <div
      css={{
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <Card css={{ flex: 1, maxWidth: '650px' }}>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe" css={{ backgroundColor: 'purple' }}>
              {image.author.slice(0, 2).toUpperCase()}
            </Avatar>
          }
          action={
            isEdit ? (
              <IconButton onClick={toggleCall(setToRemove, true)}>
                <Delete />
              </IconButton>
            ) : (
              <IconButton aria-label="settings" onClick={() => setEdit(true)}>
                <Edit />
              </IconButton>
            )
          }
          title={
            isEdit ? (
              <div>
                <TextField
                  css={{ width: '100%' }}
                  margin="none"
                  label="Title"
                  required={true}
                  type="text"
                  defaultValue={image.title}
                  onChange={e => {
                    store.titleUpdate = e.currentTarget.value;
                  }}
                  error={!image.title}
                  helperText={!image.title && 'This field is required'}
                />
              </div>
            ) : (
              image.title
            )
          }
          subheader={`created at: ${new Date(image.created).toLocaleString()}`}
        />
        <CardMedia title={image.title}>
          <img src={image.src} srcSet={image.srcset} css={{ width: '100%' }} />
        </CardMedia>
        {isEdit && (
          <CardActions css={{ flexDirection: 'row-reverse' }}>
            <IconButton color="secondary" onClick={toggleCall(setEdit, false)}>
              <CancelOutlined />
            </IconButton>
            <IconButton onClick={onSave}>
              <Save />
            </IconButton>
          </CardActions>
        )}
      </Card>
      <ConfirmDialog
        open={toRemove}
        onCancel={toggleCall(setToRemove, false)}
        onConfirm={onRemove}
        message="This action will remove image. Are you sure?"
      />
    </div>
  );
};

export const ImageDetails = withInject<RoutingMap & ImageDetailsMobxContainer>([
  {
    store: 'images',
    transform: imagesStore => ({
      getOne: imagesStore.getOneById,
      updateTitle: imagesStore.updateImageTitle,
      removeImage: imagesStore.removeImage,
    }),
  } as InjectionMapper<'images', { getOne: Function; updateTitle: Function }>,
  mapRoutingStore,
])(observer(ImageDetailsComponent));
