import React from 'react';
import {
  GridList,
  GridListTile,
  GridListTileBar,
  Typography,
} from '@material-ui/core';

const list = [
  {
    name: 'React + SSR',
    img:
      'https://github.com/facebook/create-react-app/blob/master/packages/cra-template/template/public/logo512.png?raw=true',
    web: 'https://reactjs.org/',
  },
  {
    name: 'Typesctipt',
    img: 'https://raw.githubusercontent.com/remojansen/logo.ts/master/ts.png',
    web: 'https://www.typescriptlang.org/',
  },
  {
    name: 'GraphQL',
    img: 'https://graphql.org/img/logo.svg',
    web: 'https://graphql.org/learn/',
  },
  {
    name: 'Mobx',
    web: 'https://mobx.js.org/README.html',
    img: 'https://mobx.js.org/assets/mobx.png',
  },
  {
    name: 'Apollo GraphQL',
    web: 'https://www.apollographql.com/docs/',
    img: 'https://miro.medium.com/max/3392/1*BIR94Q8MDPonvvFtsnUYLg.png',
  },
  {
    name: 'NodeJS',
    web: 'https://nodejs.org',
    img: 'https://nodejs.org/static/images/logo.svg',
  },
  {
    name: 'Jest',
    img:
      'https://d2eip9sf3oo6c2.cloudfront.net/tags/images/000/000/940/thumb/jestlogo.png',
    web: 'https://jestjs.io/',
  },
  {
    name: 'Storybook',
    web: 'https://storybook.js.org/',
    img:
      'https://repository-images.githubusercontent.com/54173593/39e57000-a3fa-11e9-83c7-953827061607',
  },
  {
    name: 'Standard Version',
    web: 'https://github.com/conventional-changelog/standard-version',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg',
  },
  {
    name: 'Babel @7',
    web: 'https://babeljs.io/',
    img:
      'https://d33wubrfki0l68.cloudfront.net/7a197cfe44548cc1a3f581152af70a3051e11671/78df8/img/babel.svg',
  },
  {
    name: 'Webpack',
    web: 'https://webpack.js.org',
    img: 'https://webpack.js.org/e0b5805d423a4ec9473ee315250968b2.svg',
  },
  {
    name: '@typescript-eslint',
    web: 'https://eslint.org/',
    img: 'https://eslint.org/assets/img/logo.svg',
  },
];

export const About = () => (
  <div>
    <Typography component="h4">Used Technologies</Typography>
    <GridList cellHeight={150} cols={1}>
      {list.map(item => (
        <a
          key={item.name}
          href={item.web}
          target="_blank"
          rel="noopener noreferrer"
          css={{ maxWidth: '150px' }}
        >
          <GridListTile css={{ border: '1px solid #ccc' }}>
            {item.img && <img src={item.img} css={{ maxWidth: '150px' }} />}
            <GridListTileBar title={item.name} />
          </GridListTile>
        </a>
      ))}
    </GridList>
  </div>
);
