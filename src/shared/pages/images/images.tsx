import React from 'react';
import {
  GridList,
  GridListTile,
  GridListTileBar,
  IconButton,
} from '@material-ui/core';
import { observer } from 'mobx-react';
import { EditOutlined } from '@material-ui/icons';

import {
  ImagesMap,
  RoutingMap,
  mapRoutingStore,
  mapImagesStore,
  useResponsive,
  withInject,
} from '#shared/utils';
import { ImageData } from '#shared/types';

const mqBetween = (start: number, end: number) =>
  `@media (min-width:${start}px) and (max-width:${end}px)`;

const resposiveLayoutConfig = [
  { query: theme => theme.breakpoints.down(400), value: 1 },
  { query: mqBetween(400, 500), value: 2 },
  { query: mqBetween(600, 700), value: 1 },
  { query: mqBetween(701, 800), value: 2 },
];

export const ImagesComponent: React.FC<{
  tileData: ImageData[];
  goTo: (url: string) => (e: React.SyntheticEvent) => void;
}> = ({ tileData, goTo }) => {
  const cols = useResponsive(resposiveLayoutConfig, 3);
  return (
    <GridList cellHeight={160} cols={cols}>
      {tileData.map(tile => (
        <a
          key={tile.src}
          onClick={goTo(`/images/${tile.id}`)}
          href={`/images/${tile.id}`}
        >
          <GridListTile>
            <img src={tile.src} srcSet={tile.srcset} alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              subtitle={<span>by: {tile.author}</span>}
              actionIcon={
                <IconButton aria-label={`info about ${tile.title}`}>
                  <EditOutlined css={{ color: 'white' }} />
                </IconButton>
              }
            />
          </GridListTile>
        </a>
      ))}
    </GridList>
  );
};

export const Images = withInject<ImagesMap & RoutingMap>([
  mapImagesStore,
  mapRoutingStore,
])(observer(ImagesComponent));
