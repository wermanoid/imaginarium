import React from 'react';
import { inject } from 'mobx-react';

import { Stores } from '#shared/stores';
import { ImageData, Omit } from '#shared/types';

export type StoreOption = keyof Stores;
export interface InjectionMapper<O extends StoreOption, T> {
  store: O;
  transform: (store: Stores[O]) => T;
}

export const createInjector = <R>(
  mappers: Array<InjectionMapper<StoreOption, unknown>>
) => (allStores: Stores) =>
  (mappers.reduce(
    (acc, mapper) => ({
      ...acc,
      ...(mapper.transform(allStores[mapper.store]) as object),
    }),
    {} as object
  ) as unknown) as R;

export interface ImagesMap {
  tileData: ImageData[];
}

export const mapImagesStore: InjectionMapper<'images', ImagesMap> = {
  store: 'images',
  transform: imagesStore => ({
    tileData: imagesStore.data,
  }),
};

export interface RoutingMap {
  goTo: (
    url: string,
    callback?: () => void
  ) => (e?: React.SyntheticEvent) => void;
  actualPath: string;
}

export const mapRoutingStore: InjectionMapper<'routing', RoutingMap> = {
  store: 'routing',
  transform: routing => ({
    goTo: (url: string, callback?: () => void) => (
      e?: React.SyntheticEvent
    ) => {
      e?.preventDefault();
      routing.push(url);
      callback?.();
    },
    actualPath: routing.location.pathname,
  }),
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const withInject = <R>(config: Array<InjectionMapper<any, any>>) => <T>(
  component: React.ComponentType<T>
) =>
  (inject(createInjector(config))(component) as unknown) as React.ComponentType<
    Omit<T, keyof R>
  >;
