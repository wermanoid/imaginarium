import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Theme } from '@material-ui/core/styles/createMuiTheme';
import { Just, Maybe, Nothing } from 'purify-ts/Maybe';
import { List } from 'purify-ts/List';

const map = <T, R>(f: (x: T) => R) => <U extends T[]>(m: U) => m.map(f);

const identity = <T>(x: T) => x;

const findMatched = List.find(<T>(item: Maybe<T>) => item.isJust());

export interface MqInput<T> {
  query: string | ((theme: Theme) => string);
  value: T;
}

const processInput = <T>(mq: MqInput<T>): Maybe<T> =>
  useMediaQuery(mq.query) ? Just(mq.value) : Nothing;

export const useResponsive = <T>(mqs: MqInput<T>[], defaultValue: T) => {
  return Just(mqs)
    .map(map(processInput))
    .chain<Maybe<T>>(findMatched)
    .chain(identity)
    .orDefault(defaultValue);
};
