import React from 'react';
import { Typography } from '@material-ui/core';

export const NavigationBarHeader: React.FC<{
  title: string;
  icon?: React.ReactElement | null;
}> = ({ title, icon }) => (
  <>
    {icon}
    <Typography variant="h6" noWrap css={{ cursor: 'default' }}>
      {title}
    </Typography>
  </>
);

NavigationBarHeader.defaultProps = {
  icon: null,
};
