import React from 'react';
import { text, select } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

import { Edit, OutlinedFlag, ModeCommentRounded } from '@material-ui/icons';
import { NavigationBar } from '#shared/atoms/navigation-bar';

import { NavigationBarHeader } from './navigation-bar-header';

storiesOf('[Molecules]|NavigationBar', module).add('default', () => (
  <div css={{ backgroundColor: '#eee' }}>
    <NavigationBar>
      <NavigationBarHeader
        title={text('Title', 'Navigation Bar Title')}
        icon={
          {
            Edit: <Edit />,
            OutlinedFlag: <OutlinedFlag />,
            ModeCommentRounded: <ModeCommentRounded />,
          }[
            select(
              'Icon',
              {
                Edit: 'Edit',
                OutlinedFlag: 'OutlinedFlag',
                ModeCommentRounded: 'ModeCommentRounded',
              },
              'Edit'
            )
          ]
        }
      />
    </NavigationBar>
  </div>
));
