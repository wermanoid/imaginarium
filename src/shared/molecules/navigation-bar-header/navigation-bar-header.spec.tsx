import React from 'react';
import { shallow, mount } from 'enzyme';

import { NavigationBarHeader } from './navigation-bar-header';

describe('navigation-bar-header', () => {
  it('should match snapshot', () => {
    const wrapper = mount(
      <NavigationBarHeader icon={<div>Icon</div>} title="Example" />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render title only', () => {
    const wrapper = shallow(<NavigationBarHeader title={'Example'} />);

    expect(wrapper.children()).toHaveLength(1);
    expect(wrapper.text()).toEqual('Example');
  });

  it('should render title and icon', () => {
    const wrapper = shallow(
      <NavigationBarHeader
        title={'Example'}
        icon={<div className="icon">Icon</div>}
      />
    );

    const icon = wrapper.find('.icon');

    expect(wrapper.children()).toHaveLength(2);
    expect(icon).toHaveLength(1);
    expect(icon.text()).toBe('Icon');
  });
});
