import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { Theme } from '@material-ui/core/styles/createMuiTheme';

import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Divider } from '@material-ui/core';

export interface LayoutProps {
  header?: React.ComponentType;
  footer?: React.ComponentType;
}

const flexColumn = {
  display: 'flex',
  flexDirection: 'column' as const,
};

const screenView = {
  ...flexColumn,
  backgroundColor: 'rgba(0, 0, 0, 0.1)',
  height: '100vh',
};

const viewport = {
  ...flexColumn,
  minHeight: 'calc(100% - 51px)',
  padding: '1rem',
  backgroundColor: '#fff',
};

const drawerWidth = '250px';

const overflowView = {
  height: '100%',
  overflow: 'auto',
};

const useStyles = makeStyles((theme: Theme) => ({
  layoutContainer: {
    marginTop: '50px',
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth})`,
      marginLeft: drawerWidth,
    },
  },
}));

const Layout: React.SFC<LayoutProps> = ({ children, header, footer }) => {
  const { layoutContainer } = useStyles();

  return (
    <>
      <CssBaseline />
      <div css={screenView}>
        {header && <section>{React.createElement(header, null)}</section>}
        <section css={overflowView}>
          <Container maxWidth="lg" className={layoutContainer}>
            <Typography component="div" css={viewport}>
              <div css={{ flex: 1, marginBottom: '25px' }}>{children}</div>
              <Divider />
              {footer && React.createElement(footer, null)}
            </Typography>
          </Container>
        </section>
      </div>
    </>
  );
};

export default Layout;
