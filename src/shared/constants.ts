export enum RoutesEnum {
  Home = '/',
  Images = '/images',
  ImageDetails = '/images/:id',
  About = '/about',
}
