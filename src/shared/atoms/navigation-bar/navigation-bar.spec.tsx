import React from 'react';
import { mount } from 'enzyme';

import { NavigationBar } from './navigation-bar';

describe('navigation-bar', () => {
  it('should match snapshot', () => {
    const wrapper = mount(<NavigationBar>content</NavigationBar>);

    expect(wrapper).toMatchSnapshot();
  });

  it('should render children', () => {
    const wrapper = mount(<NavigationBar>content</NavigationBar>);

    expect(wrapper.text()).toEqual('content');
  });
});
