import React from 'react';
import { text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

import { NavigationBar } from './navigation-bar';

storiesOf('[Atom]|NavigationBar', module).add('default', () => (
  <div css={{ backgroundColor: '#eee' }}>
    <NavigationBar>{text('Content', 'Navigation Bar Content')}</NavigationBar>
  </div>
));
