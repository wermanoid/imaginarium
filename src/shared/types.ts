export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

export interface ImageData {
  id: string;
  src: string;
  srcset: string;
  title: string;
  author: string;
  created: Date | string;
}
