import { ImageData } from '#shared/types';
import { gql } from 'apollo-server-express';

import { DataStorage } from './datasource';

const storage = new DataStorage();

const imagesResolver = storage.query;

const imageResolver = (_: unknown, args: Pick<ImageData, 'id' | 'title'>) =>
  storage.query().find(item => item.id === args.id);

const removeImageMutation = (root: unknown, args: Pick<ImageData, 'id'>) =>
  storage.removeImageById(args.id);

const updateTitleMutation = (
  _: unknown,
  { upd }: { upd: Pick<ImageData, 'id' | 'title'> }
) => storage.updateTitle(upd);

export const typeDefs = gql`
  type Image {
    id: String!
    title: String!
    author: String
    src: String!
    srcset: String
    created: String
  }

  input UpdateTitleArgs {
    id: String!
    title: String!
  }

  type Query {
    images: [Image]
    image(id: String!): Image
  }

  type Mutation {
    updaetTitle(upd: UpdateTitleArgs!): Boolean
    removeImage(id: String!): Boolean
  }
`;

export const resolvers = {
  Query: {
    images: imagesResolver,
    image: imageResolver,
  },
  Mutation: {
    updaetTitle: updateTitleMutation,
    removeImage: removeImageMutation,
  },
};
