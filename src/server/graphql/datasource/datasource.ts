import faker from 'faker';
import { uniqueId } from 'lodash/fp';
import fs from 'fs';
import path from 'path';
import { Maybe } from 'purify-ts/Maybe';

import { ImageData } from '#shared/types';

const updateDBJson = (newState: object) => {
  const dbInfo = path.resolve(process.cwd(), './db/data.json');
  fs.writeFile(
    dbInfo,
    JSON.stringify(newState, null, 2),
    { flag: 'w' },
    () => {}
  );
};

function* fakeStore(): IterableIterator<ImageData & { files: string[] }> {
  const dbInfo = path.resolve(process.cwd(), './db/data.json');

  const inMemory = fs.readFileSync(dbInfo).toString();

  const { images } = JSON.parse(inMemory);
  for (let i = 0; i < images.length; i++) {
    const { id, title, author, created, src, srcset, files } = images[i];
    yield {
      src,
      srcset,
      files,
      id: id ?? uniqueId('IMG-'),
      title: title ?? faker.lorem.sentence(5),
      author: author ?? `${faker.name.firstName()} ${faker.name.lastName()}`,
      created: created ?? faker.date.past().toISOString(),
    };
  }
}

const aggregateTo = <T>(gen: IterableIterator<T>, acc: T[]): T[] => {
  const { value, done } = gen.next();
  if (!done) return aggregateTo(gen, [...acc, value]);
  return acc;
};

export class DataStorage {
  private images: ImageData[];

  constructor(generator: IterableIterator<ImageData> = fakeStore()) {
    this.images = aggregateTo(generator, []);
    updateDBJson({ images: this.images });
  }

  public query = () => {
    return this.images;
  };

  public removeImageById = (id: string) =>
    Maybe.fromNullable(this.images.find(item => item.id === id))
      .map(toRemove => {
        ((toRemove as unknown) as { files: string[] }).files.forEach(file =>
          fs.unlink(path.resolve(process.cwd(), `./${file}`), () => void 0)
        );
        return toRemove;
      })
      .map(toRemove => {
        this.images = this.images.filter(item => item.id !== toRemove.id);
        return this.images;
      })
      .map(result => updateDBJson({ images: result }))
      .map(() => true)
      .extract();

  public updateTitle = (args: { id: string; title: string }) => {
    process.stdout.write(JSON.stringify({ args }));
    return Maybe.fromNullable(args)
      .chain(update =>
        Maybe.fromNullable(this.images.find(({ id }) => id === update.id))
      )
      .map(item => ({ ...item, title: args.title }))
      .map(updated => {
        this.images = this.images.map(item =>
          item.id === updated.id ? updated : { ...item }
        );
        return this.images;
      })
      .map(result => updateDBJson({ images: result }))
      .map(() => true)
      .extract();
  };
}
